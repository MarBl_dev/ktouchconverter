package ch.mbdev.ktouchconverter.test


import ch.mbdev.ktouchconverter.model.Course
import ch.mbdev.ktouchconverter.xml.CourseWriter
import org.scalatest._
import org.scalatest.matchers._
import ch.mbdev.ktouchconverter.model.Lesson
import ch.mbdev.ktouchconverter.id.IdGenerator
import ch.mbdev.ktouchconverter.xml.CourseParser

class CourseParserSpec extends FlatSpec with ShouldMatchers{

	class SimpleIdGen extends IdGenerator{
	  var counter = 0
	  
	  def generateId():String = {
	    counter += 1
	    return "id"+counter
	  }
	}
  
  "parse" should "create valid course objects" in {
    val path = System.getProperty("user.dir") + "/data/test/testimport.xml"
    System.out.println(path)
    val parser = new CourseParser()
    parser.idGenerator = new SimpleIdGen()
    val res = parser.parseCourse(path)
    
    res.id should equal ("id1")
    res.description should equal ("desc")
    res.keyboardLayout should equal ("de")
    res.suggestedFont should equal ("Arial")
    res.title should equal ("title for this course")
    res.lessons.isDefined should equal (true)
    res.lessons.get should have size (2)
    
    val lesson1 = res.lessons.get(0)
    lesson1.id should equal ("id2")
    lesson1.newCharacters should equal ("le")
    lesson1.title should equal ("lesson 1 title")
    lesson1.text should equal ("multiline text \n of this lesson 1")
    
    val lesson2 = res.lessons.get(1)
    lesson2.id should equal ("id3")
    lesson2.newCharacters should equal ("ua")
    lesson2.title should equal ("lesson 2 title")
    lesson2.text should equal ("multiline text \n of this lesson 2")
  }
  
  it should "create an empty lesson course" in {
    val path = System.getProperty("user.dir") + "/data/test/testimport_nolessons.xml"
    val parser = new CourseParser()
    parser.idGenerator = new SimpleIdGen()
    val res = parser.parseCourse(path)
    
    res.id should equal ("id1")
    res.description should equal ("desc")
    res.keyboardLayout should equal ("de")
    res.suggestedFont should equal ("Arial")
    res.title should equal ("title for this course")
    res.lessons should equal (Option.empty)
    
  }
  
  it should "throw an exception an empty path" in{
    val parser = new CourseParser()
    evaluating {parser.parseCourse(null)} should produce [IllegalArgumentException]
    evaluating {parser.parseCourse("")} should produce [IllegalArgumentException]
  }
}