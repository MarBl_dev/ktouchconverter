package ch.mbdev.ktouchconverter.test


import ch.mbdev.ktouchconverter.model._
import ch.mbdev.ktouchconverter.xml.CourseWriter
import org.scalatest._
import org.scalatest.matchers._
import ch.mbdev.ktouchconverter.id.IdGenerator
import ch.mbdev.ktouchconverter.xml.CourseParser
import org.scalamock.scalatest.MockFactory
import org.scalamock.ProxyMockFactory
import ch.mbdev.ktouchconverter.converter.Converter
import ch.mbdev.ktouchconverter.xml.XMLCourseWriter
import org.scalamock.GeneratedMockFactoryBase
import org.scalamock.ClassLoaderStrategy
import ch.mbdev.ktouchconverter.xml.XMLCourseParser

class ConverterSpec extends FlatSpec with ShouldMatchers with MockFactory with ProxyMockFactory{
	
  val inPath = "/path/to/inputfile.xml"
  val outPath = "/path/to/outfile.xml"
 
  
  "convert" should "convert the xml files" in {
	  val parserMock = mock[XMLCourseParser]
	  val writerMock = mock[XMLCourseWriter]
    
	  parserMock expects 'parseCourse withArgs (inPath) onCall{x:AnyRef => Course()}
	  writerMock expects 'writeToXml where {x:Course => x.keyboardLayout == "de"} onCall{x:AnyRef => "<tag></tag>"}
	  writerMock expects 'writeToXMLFile withArgs (outPath, "<tag></tag>") once
	  
	  Converter.courseParser = parserMock
	  Converter.courseWriter = writerMock
	  Converter.convert(inPath, outPath, "de")
  }
  
  it should "throw illegalArgumentExceptions on invalid paths" in {
    val inPath = "/path/to/inputfile.xml"
	val outPath = "/path/to/outfile.xml"
    evaluating {Converter.convert(null, outPath)} should produce [IllegalArgumentException]
    evaluating {Converter.convert(inPath, "")} should produce [IllegalArgumentException]
    
  }
  
  it should "correct null as keyboardLayout to empty string" in {
    val inPath = "/path/to/inputfile.xml"
	val outPath = "/path/to/outfile.xml"
	
	val parserMock = mock[XMLCourseParser]
	val writerMock = mock[XMLCourseWriter]

    Converter.courseParser = parserMock
	Converter.courseWriter = writerMock
	
	parserMock expects 'parseCourse onCall{x:String => Course()}
    writerMock expects 'writeToXml where{x:Course => x.keyboardLayout == ""} onCall{x:AnyRef => "<tag></tag>"}
    writerMock expects 'writeToXMLFile
	Converter.convert(inPath, outPath, null)
  }
	
  
}