package ch.mbdev.ktouchconverter.test


import ch.mbdev.ktouchconverter.model.Course
import ch.mbdev.ktouchconverter.xml.CourseWriter
import org.scalatest._
import org.scalatest.matchers._
import ch.mbdev.ktouchconverter.model.Lesson
import java.io.File
import scala.xml.XML
import java.io.FileNotFoundException

class CourseWriterSpec extends FlatSpec with ShouldMatchers{

  "writeToXML" should "produce valid xml" in {
    val lesson1 = Lesson()
    lesson1.id = "lessonid1"
    lesson1.newCharacters = "le"
    lesson1.title = "lesson 1 title"
    lesson1.text = "multiline text \n of this lesson 1"
    
    val lesson2 = Lesson()
    lesson2.id = "lessonid2"
    lesson2.newCharacters = "ua"
    lesson2.title = "lesson 2 title"
    lesson2.text = "multiline text \n of this lesson 2"
    
    var lessons = List[Lesson]()
    lessons = lessons :+ lesson1
    lessons = lessons :+ lesson2
    
    val c = new Course()
    c.id = "id"
    c.description = "desc"
    c.keyboardLayout = "de"
    c.suggestedFont = "Arial"
    c.title = "title for this course"
    c.lessons = Option(lessons);
      
    val writer = new CourseWriter()
    System.out.print(c.toXML(writer))
    
    
    val xmlRes = c.toXML(writer)
    xmlRes.label should equal ("course")
    (xmlRes \ "id").head.text should equal ("id")
    (xmlRes \ "description").head.text should equal ("desc")
    (xmlRes \ "keyboardLayout").head.text should equal ("de")
    (xmlRes \ "lessons" \ "lesson") should have size (2)
    val lesson1Part = (xmlRes \ "lessons" \ "lesson")(0)
    val lesson2Part = (xmlRes \ "lessons" \ "lesson")(1)
    (lesson1Part \ "id").head.text should equal ("lessonid1")
    (lesson1Part \ "title").head.text should equal ("lesson 1 title")
    (lesson1Part \ "newCharacters").head.text should equal ("le")
    (lesson1Part \ "text").head.text should equal ("multiline text \n of this lesson 1")
    (lesson2Part \ "id").head.text should equal ("lessonid2")
    (lesson2Part \ "title").head.text should equal ("lesson 2 title")
    (lesson2Part \ "newCharacters").head.text should equal ("ua")
    (lesson2Part \ "text").head.text should equal ("multiline text \n of this lesson 2")
    	
  }
  
  it should "write xml without lessons tags if there arent any" in {
    val c = new Course()
    c.id = "id"
    c.description = "desc"
    c.keyboardLayout = "de"
    c.suggestedFont = "Arial"
    c.title = "title for this course"
    c.lessons = Option.empty;
    val writer = new CourseWriter()
    System.out.print(c.toXML(writer))
    val xmlRes = writer.writeToXml(c)
    xmlRes.label should equal ("course")
    (xmlRes \ "id").head.text should equal ("id")
    (xmlRes \ "description").head.text should equal ("desc")
    (xmlRes \ "keyboardLayout").head.text should equal ("de")
    (xmlRes \ "lessons" \ "lesson") should have size (0)
   
  }
  
  "writeToXMLFile" should "create a valid xml file" in{
    val writer = new CourseWriter()
    val path = System.getProperty("user.dir") + "/data/test/testexport.xml"
    val xml = <test><file><content>text</content></file><file></file></test>
    
    writer.writeToXMLFile(path, xml)
    XML.loadFile(path) should equal (xml)
  }
  
  it should "throw IllegalArgumentException on null parameter" in {
    val writer = new CourseWriter()
    val path = System.getProperty("user.dir") + "/data/test/testexport.xml"
    val xml = <test><file><content>text</content></file><file></file></test>
      
    evaluating { writer.writeToXMLFile(null, xml) } should produce [IllegalArgumentException]
    evaluating { writer.writeToXMLFile(path, null) } should produce [IllegalArgumentException]
    evaluating { writer.writeToXMLFile("", null) } should produce [IllegalArgumentException]
    evaluating { writer.writeToXMLFile("/invalid/path", xml) } should produce [FileNotFoundException]
  }
  
  
}