# Welcome

Welcome to the KTouchConverter wiki!
KTouchConverter is a command line converter tool for KTouch course files. If you have custom KTouch course files for KTouch version < 1.7, you can convert these with this tool to make them work with KTouch v1.7.x.  
This project is completely independent from KTouch 

#Get the converter

You can download the converter in the download section of this page.  
If you have scala installed, download the [_scala version](http://bitbucket.org/fczfan007/ktouchconverter/downloads/KTouchConverter_scala.jar) otherwise choose the [java version](http://bitbucket.org/fczfan007/ktouchconverter/downloads/KTouchConverter_scala.jar)

##Prerequisites
- The .jar file of the converter
- \>= JRE 1.6 (Java Runtime Environment) or scala >= 2.9.2
- an .xml file of a course you want to convert

##Installation
No installation is required. Just keep the .jar file.

#Use the converter

##Start
To run the KTouchConverter.jar:

- Open a console
- start the converter with
```
$ java -jar KTouchConverter.jar
```

To run the KTouchConverter_scala.jar:

- Open a console
- start the converter with
```
$ scala KTouchConverter_scala.jar
```

##Usage
KTouchConverter <inFile.xml> <outFile.xml> [options]

Options:  
-l:<keyboardLayout>      Specifies a keyboardlayout for the course

##Example
```
$ scala KTouchConverter_scala.jar /tmp/oldfile.xml /tmp/newfile.xml
$ java -jar KTouchConverter.jar /tmp/oldfile.xml /tmp/newfile.xml
```


