package ch.mbdev.ktouchconverter.id

import java.util.UUID

class GuidGenerator extends IdGenerator {

  def generateId(): String = { 
	UUID.randomUUID().toString()
  }

}