package ch.mbdev.ktouchconverter.model

import ch.mbdev.ktouchconverter.xml.XMLCourseWriter

case class Course() {
	var title:String = ""
	var id:String = ""
	var suggestedFont:String = ""
	var keyboardLayout:String = ""
	var description:String = ""
	var lessons:Option[List[Lesson]] = Option.empty
	
	def toXML(writer:XMLCourseWriter) = {
	  writer.writeToXml(this)
	}
}