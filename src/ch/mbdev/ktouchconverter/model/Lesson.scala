package ch.mbdev.ktouchconverter.model

case class Lesson() {
	var title:String = ""
	var id:String = ""
	var newCharacters:String = ""
	var text:String = ""
}