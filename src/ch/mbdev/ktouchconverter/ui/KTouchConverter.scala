package ch.mbdev.ktouchconverter.ui

import ch.mbdev.ktouchconverter.converter.Converter

object KTouchConverter extends App {
	if(args.length < 2) {
	  System.out.println("Error: please provide at least input and output file!")
	  printUsage();
	  System.exit(1)
	}
	private val inFile = args(0)
	private val outFile = args(1)
	private var keyboardLayout = ""
	
	if(args.length > 2){
	  setOptions()
	}
	
	try{
		Converter.convert(inFile, outFile, keyboardLayout)
		System.out.println("Conversion successful")
	}catch{
	  case e:Exception => e.printStackTrace()
	  case _ => System.out.println("Fatal Error!")
	}
	exit(0)
	
	private def printUsage()= {
	  System.out.println("Usage:")
	  System.out.println("KTouchKonverter <inFile.xml> <outFile.xml> [options]")
	  System.out.println("Options:")
	  System.out.println("-l:<keyboardLayout> \t Specifies a keyboardlayout for the course")
	}
	
	private def onInvalidOption(option:String){
	  println("options must start with '-' and only have one letter"); 
	  printUsage
	  exit(1)
	}
	
	private def onUnknownOption(option:String){
	  System.out.println("Error: Unknown option "+option)
	  printUsage()
	  System.exit(1)
	}
	
	private def setLayout(layout:String){
	  keyboardLayout = if (layout == null) {
	    System.out.println("Warning: you specified an empty layout [-l:<layout>]")
	    ""
	  }else{
	    layout
	  }
	  
	}
	
	private def setOptions(){
	  val Option = """^-([a-z])(:(.*))?$""".r
	  
	  args.filter{_.startsWith("-")}.foreach{
	    _ match { 
	      case Option("l",_,layout) => setLayout(layout)
	      case Option(x,_,_) => onUnknownOption(x)
	      case _ @ x =>  onInvalidOption(x)
	    }
	  }
	}
	
}