package ch.mbdev.ktouchconverter.converter

import ch.mbdev.ktouchconverter.xml.CourseParser
import ch.mbdev.ktouchconverter.xml.CourseWriter
import ch.mbdev.ktouchconverter.xml.XMLCourseWriter
import ch.mbdev.ktouchconverter.xml.XMLCourseParser

object Converter{
  
	var courseWriter:XMLCourseWriter = new CourseWriter
	var courseParser:XMLCourseParser = new CourseParser
	
	def convert(importPath:String, exportPath:String, keyboardLayout:String = "") = {
	  require(importPath != null && importPath != "", "importPath must not be empty!")
	  require(exportPath != null && exportPath != "", "exportPath must not be empty!")
	  
	  val course = courseParser.parseCourse(importPath)
	  course.keyboardLayout = if (keyboardLayout == null) "" else keyboardLayout
	  val xml = courseWriter.writeToXml(course)
	  courseWriter.writeToXMLFile(exportPath, xml)
	  
	  
	}
}