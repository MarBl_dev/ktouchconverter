package ch.mbdev.ktouchconverter.xml

import scala.xml.XML
import ch.mbdev.ktouchconverter.model.Course
import ch.mbdev.ktouchconverter.model.Lesson
import ch.mbdev.ktouchconverter.id.GuidGenerator
import ch.mbdev.ktouchconverter.id.IdGenerator

class CourseParser() extends XMLCourseParser{
	
	var idGenerator:IdGenerator = new GuidGenerator()
	
	def parseCourse(path:String):Course = {
	  require(path != null && path != "", "path must not be empty!")
	  
	  val xml = XML.loadFile(path)
	  val course = Course()
	  course.id = idGenerator.generateId
	  course.title = (xml \ "Title").map(_.text).mkString
	  course.description = (xml \ "Comment").map(_.text).mkString
	  course.suggestedFont = (xml \ "FontSuggestions").map(_.text).mkString
	  
	  val xmlLessons = (xml \ "Levels" \ "Level")
	  val lessons = xmlLessons.map{xmlLesson => 
	  	val lesson = Lesson()
	    lesson.title = (xmlLesson \ "LevelComment").map(_.text).mkString
	    lesson.id = idGenerator.generateId
	    lesson.newCharacters = (xmlLesson \ "NewCharacters").map(_.text).mkString
	    lesson.text = (xmlLesson \ "Line").map(_.text).mkString("\n")
	    lesson
	  }.toList
	  course.lessons = if(lessons.isEmpty) Option.empty else Option(lessons)
	  
	  return course
	}
  
	
}