package ch.mbdev.ktouchconverter.xml

import ch.mbdev.ktouchconverter.model.Lesson
import scala.xml.Node

class LessonWriter extends XMLLessonWriter{
	def writeToXML(lesson:Lesson):Node = {
		<lesson>
			<id>{lesson.id}</id>
			<title>{lesson.title}</title>
			<newCharacters>{lesson.newCharacters}</newCharacters>
			<text>{lesson.text}</text>
		</lesson>
	  
	}
}