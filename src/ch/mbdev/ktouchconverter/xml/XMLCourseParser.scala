package ch.mbdev.ktouchconverter.xml

import ch.mbdev.ktouchconverter.model.Course

trait XMLCourseParser {
	def parseCourse(path:String):Course
}