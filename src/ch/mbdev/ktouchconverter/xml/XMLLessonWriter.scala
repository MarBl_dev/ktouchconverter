package ch.mbdev.ktouchconverter.xml

import ch.mbdev.ktouchconverter.model.Lesson
import scala.xml.Node
import scala.xml.Elem

trait XMLLessonWriter {
	def writeToXML(lesson:Lesson):Node
}