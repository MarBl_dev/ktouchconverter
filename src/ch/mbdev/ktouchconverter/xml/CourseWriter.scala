package ch.mbdev.ktouchconverter.xml

import ch.mbdev.ktouchconverter.model.Course
import scala.xml.Node
import scala.xml.XML
import scala.xml.Elem

class CourseWriter extends XMLCourseWriter {
    
  private def lessonWriter():XMLLessonWriter = {
    new LessonWriter()
  }
  
    
  def writeToXml(course:Course):Elem = {
		<course>
    		<id>{course.id}</id>
    		<title>{course.title}</title>
    		<description>{course.description}</description>
    		<keyboardLayout>{course.keyboardLayout}</keyboardLayout>
    		<lessons>
    			{
			   course.lessons match {
				  case None => null
				  case Some(x) => x.map(l => lessonWriter.writeToXML(l))
			   }
			  
			}
    		</lessons>
    	</course>
  }
  
  def writeToXMLFile(path:String,xml:Elem) = {
    require(path != null && path != "", "path must not be empty")
    require(xml != null && xml != "", "xml must not be empty")
    
    XML.saveFull(path, xml,"UTF-8", true, null)
  }
}