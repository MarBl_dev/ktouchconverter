package ch.mbdev.ktouchconverter.xml

import ch.mbdev.ktouchconverter.model.Course
import scala.xml.Elem

trait XMLCourseWriter {
	def writeToXml(course:Course):Elem
	def writeToXMLFile(path:String,xml:Elem)
}